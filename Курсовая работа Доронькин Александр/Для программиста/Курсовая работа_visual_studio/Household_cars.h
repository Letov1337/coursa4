#ifndef GAZ_H
#define GAZ_H
#include "One_type.h"
#include "Cars.h"
using namespace AVTO;
class Household_cars: public One_type
{
    public:
        Household_cars();
        void set_vgaz();
        int get_vgaz();
        void Avto_inf();
        int Loading(Household_cars &x);
        void Save(Household_cars &x);
        friend std::ostream& operator<< (std::ostream &out, Household_cars &x)
{
    out << std:: endl << "��������:" << " " << x.get_name() << std:: endl <<
        "�����:" << " " << x.get_marka() << std:: endl <<
        "������:" << " " << x.get_country() <<std:: endl <<
        "������������  ��������:" << " " << x.get_speed() << " ��/�" << std:: endl <<
        "����� �� 100 ��/�:" << " " << x.get_speedup() << std:: endl <<
        "��� ������:" << " " << x.get_typecuzov() << std:: endl <<
        "���-�� �.�.: " << x.get_kolvhorse()<< std:: endl <<
        "���: " << x.get_god() << std:: endl <<
        "�������� ����������� ������� : " << x.get_vgaz() << std:: endl<< std:: endl;
        return out;
}
    private:
        int vgaz;
};

#endif // GAZ_H
