#ifndef DIZEL_H
#define DIZEL_H
#include "Two_type.h"

class Sport_cars: public Two_type
{
    public:
        Sport_cars();
        void  set_typenaduf();
        void set_vibros();
        int get_vibros();
        const char* get_typenaduf();
        void Avto_inf();
        int Loading(Sport_cars &x);
        void Save(Sport_cars &x);
        friend std::ostream& operator<< (std::ostream &out, Sport_cars &x)
{
    out << std:: endl << "��������:" << " " << x.get_name() << std:: endl <<
        "�����:" << " " << x.get_marka() << std:: endl <<
        "������:" << " " << x.get_country() <<std:: endl <<
        "������������  ��������:" << " " << x.get_speed() << " ��/�" << std:: endl <<
        "����� �� 100 ��/�:" << " " << x.get_speedup() << std:: endl <<
        "��� ������:" << " " << x.get_typecuzov() << std:: endl <<
        "���-�� �.�.: " << x.get_kolvhorse()<< std:: endl <<
        "���: " << x.get_god() << std:: endl <<
        "������� ������� : " << x.get_typenaduf() << std:: endl<<
        "������� CO2 : " << x.get_vibros() << std:: endl << std:: endl;
        return out;
}
    private:
        char typenaduf[50];
        int vibros;
};

#endif // DIZEL_H
