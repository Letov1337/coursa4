#ifndef ELEKTRO_H
#define ELEKTRO_H
#include "Two_type.h"

class Truck_cars: public Two_type
{
    public:
        Truck_cars();
        void set_battery();
        void set_xod();
        int get_battery();
        int get_xod();
        void Avto_inf();
        int Loading(Truck_cars &x);
        void Save(Truck_cars &x);
        friend std::ostream& operator<< (std::ostream &out, Truck_cars &x)
{
    out << std:: endl << "��������:" << " " << x.get_name() << std:: endl <<
        "�����:" << " " << x.get_marka() << std:: endl <<
        "������:" << " " << x.get_country() <<std:: endl <<
        "������������  ��������:" << " " << x.get_speed() << " ��/�" << std:: endl <<
        "����� �� 100 ��/�:" << " " << x.get_speedup() << std:: endl <<
        "��� ������:" << " " << x.get_typecuzov() << std:: endl <<
        "���-�� �.�.: " << x.get_kolvhorse()<< std:: endl <<
        "���: " << x.get_god() << std:: endl <<
        "������� ������� ��������� ����: " << x.get_xod() << std:: endl<<
        "������� �������� ��������� � ��/� :" << x.get_battery() << std:: endl << std:: endl;
        return out;
}
    private:
        int battery;
        int xod;
};

#endif //
