#ifndef INOMARK_H
#define INOMARK_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <windows.h>
#include "Cars.h"
using namespace AVTO;
class Two_type: public Avto
{
    public:
        Two_type();
        void  set_country();
        const char* get_country();
    private:
        char country[50];
};
#endif // INOMARK_H
