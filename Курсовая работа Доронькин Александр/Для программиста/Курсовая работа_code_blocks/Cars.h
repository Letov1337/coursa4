#ifndef AVTO_H
#define AVTO_H
#include <iostream>
#include <fstream>
#include <iomanip>
#include <windows.h>


namespace AVTO
{
class Avto
{

private:
    char typecuzov[50]; //char.....[]
    char marka[50];
    char name[50];
    int kolvhorse;
    int speed;
    int god;
    float speedup;
public:
    Avto();

virtual void Avto_inf() = 0;
void set_marka();
void set_name();
void set_speedup();
void set_typecuzov();
void set_speed();
void set_kolvhorse();
void set_god();
void  set_day();
const char* get_marka();
const char* get_name();
const char* get_typecuzov();
float get_speedup();
int get_speed();
int get_god();
int get_kolvhorse();
};
}

#endif // AVTO_H
