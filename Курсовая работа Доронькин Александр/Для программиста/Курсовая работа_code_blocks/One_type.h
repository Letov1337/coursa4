#ifndef OTECH_H
#define OTECH_H
#include "Cars.h"
using namespace AVTO;
class One_type: public Avto
{
    public:
        One_type();
        const char* get_country();
    private:
        const char* country;
};

#endif // OTECH_H
