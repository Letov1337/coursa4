#ifndef BENZIN_H
#define BENZIN_H
#include "One_type.h"
#include "Cars.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <windows.h>
using namespace AVTO;
class Family: public One_type
{
    public:
        Family();
        void set_stepen();
        void set_pitan();
        int get_stepen();
        const char* get_pitan();
        void Avto_inf();
        int Loading(Family &x);
        void Save(Family &x);
        friend std::ostream& operator<< (std::ostream &out, Family &x)
{
    out << std:: endl << "��������:" << " " << x.get_name() << std:: endl <<
        "�����:" << " " << x.get_marka() << std:: endl <<
        "������:" << " " << x.get_country() <<std:: endl <<
        "������������  ��������:" << " " << x.get_speed() << " ��/�" << std:: endl <<
        "����� �� 100 ��/�:" << " " << x.get_speedup() << std:: endl <<
        "��� ������:" << " " << x.get_typecuzov() << std:: endl <<
        "���-�� �.�.: " << x.get_kolvhorse()<< std:: endl <<
        "���: " << x.get_god() << std:: endl <<
        "���-�� ��������� : " << x.get_stepen() << std:: endl <<std:: endl;
        return out;
}
    private:
        int stepen;
        char pitan[50];

};
#endif // BENZIN_H
