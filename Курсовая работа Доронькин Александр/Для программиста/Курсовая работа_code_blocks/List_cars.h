#pragma once
#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <cassert>
#include <fstream>

using namespace std;

namespace LIST
{
template <typename T>
class List
{
    public:
        List();
        ~List();
    //�������� �������� � ������
	void pop_front();

	//���������� �������� � ����� ������
	void push_back(T data);

	// �������� ������
	void clear();
	//���������� �������� � ������ ������
	void push_front(T data);
	//�������� �������� � ������ �� ���������� �������
	void removeAt(int index);

	//�������� ���������� �������� � ������
	void pop_back();
	void Print();
	int Load_List(List<T> &object,const char* namefile);
	void Save_List(List<T> object,const char* namefile);
	int Sizee();
	void Poisk(const char* index1,const char* index2);
	void Poisk (int pos);
    private:
        template <typename T1>
        class Node
        {
        public:
            Node * pNext;
            T1 data;

            Node(T1 data = T1(), Node *pNext = NULL)
            {
                this->data = data;
                this->pNext = pNext;
            }
        };
        int Size;
        Node<T> *head;
};
}
#endif // LIST_H
