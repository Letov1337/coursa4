
#ifndef MENU_H
#define MENU_H
#include <iostream>
#include <fstream>
#include <iomanip>
#include <windows.h>
#include "Cars.h"
#include "Two_type.h"
#include "One_type.h"
#include "Family_cars.h"
#include "Sport_cars.h"
#include "Truck_cars.h"
#include "Household_cars.h"
#include "List_cars.h"
#include "List_cars.cpp"


using namespace AVTO;
using namespace LIST;
namespace MENU
{
class Menu
{
public:
    Menu();

    void Search3(List <Family> benz1, List <Sport_cars> diz1, List <Truck_cars> ele1, List <Household_cars> gz1);

};
}

#endif // MENU_H
