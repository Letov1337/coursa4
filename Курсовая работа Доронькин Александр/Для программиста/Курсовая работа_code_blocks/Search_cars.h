#ifndef SEARCH_H
#define SEARCH_H


#include <iostream>
#include <fstream>
#include <iomanip>
#include <windows.h>
#include "Cars.h"
#include "Family_cars.h"
#include "List_cars.cpp"
#include "One_type.h"
#include "Two_type.h"
#include "Truck_cars.h"
#include "Household_cars.h"
#include "Sport_cars.h"
#include "List_cars.h"



using namespace AVTO;
using namespace LIST;
namespace SEARCH
{
    class Search1
{
    public:
        Search1();
        int Poisk(List <Family> object);
        int Poisk(List <Sport_cars> object);
        int Poisk(List <Truck_cars> object);
        int Poisk(List <Household_cars> object);
        int Poisk2(List <Family> object);
        int Poisk2(List <Sport_cars> object);
        int Poisk2(List <Truck_cars> object);
        int Poisk2(List <Household_cars> object);
};
}
#endif // SEARCH_H

